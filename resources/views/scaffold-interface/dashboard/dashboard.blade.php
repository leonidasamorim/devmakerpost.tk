@extends('scaffold-interface.layouts.app')
@section('title','Dashboard')
@section('content')
	<section class="content-header">
		<h1>
			Dashboard
			<small>Painel de controle</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-aqua">
					<div class="inner">
						<h3>{{$users}}</h3>
						<p>Usuários</p>
					</div>
					<div class="icon">
						<i class="ion ion-person-stalker"></i>
					</div>
					<a href="{{url('users')}}" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
			<div class="col-lg-3 col-xs-6">
				<!-- small box -->
				<div class="small-box bg-red">
					<div class="inner">
						<h3>{{$posts}}</h3>
						<p>Posts</p>
					</div>
					<div class="icon">
						<i class="fa fa-book"></i>
					</div>
					<a href="{{url('dashboard/post')}}" class="small-box-footer">Mais informações  <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>


		</div>
	</section>
@endsection
