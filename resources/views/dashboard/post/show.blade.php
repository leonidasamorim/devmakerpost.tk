@extends('scaffold-interface.layouts.app')
@section('title','Show')
@section('content')

<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3>
                Dados do post
            </h3>
        </div>
        <div class="box-body">
            <table class = 'table table-bordered'>
                <thead>
                    <th>Campo</th>
                    <th>Valor</th>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <b><i>Título: </i></b>
                        </td>
                        <td>{!!$post->titulo!!}</td>
                    </tr>
                    <tr>
                        <td>
                            <b><i>Descrição: </i></b>
                        </td>
                        <td>{!!$post->descricao!!}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
</section>
@endsection
