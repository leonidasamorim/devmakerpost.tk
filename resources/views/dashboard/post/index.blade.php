@extends('scaffold-interface.layouts.app')
@section('title','Index')
@section('content')

<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3>
                Posts
            </h3>
            <a href="/dashboard/post/create" class = "btn btn-success"><i class="fa fa-plus fa-md" aria-hidden="true"></i> Novo</a>
        </div>
        <table class = "table table-striped table-bordered table-hover" style = 'background:#fff'>
            <thead>
                <th>Título</th>
                <th>Descrição</th>
                <th>Ações</th>
            </thead>
            <tbody>
                @foreach($posts as $post)
                <tr>
                    <td>{!!$post->titulo!!}</td>
                    <td>{!!$post->descricao!!}</td>
                    <td>
                        <a href = '/dashboard/post/{!!$post->id!!}' class = 'viewShow btn btn-warning btn-xs'><i class="fa fa-file-text-o" aria-hidden="true"></i></a>
                        <a href = '/dashboard/post/{!!$post->id!!}/edit' class = 'viewEdit btn btn-primary btn-xs'><i class="fa fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a data-toggle="modal" data-target="#myModal" class = 'delete btn btn-danger btn-xs' data-link = "/dashboard/post/{!!$post->id!!}/deleteMsg" ><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {!! $posts->render() !!}
    </div>
</div>
</section>
@endsection
