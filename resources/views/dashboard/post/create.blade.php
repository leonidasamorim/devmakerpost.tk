@extends('scaffold-interface.layouts.app')
@section('title','Create')
@section('content')

<section class="content">
    <div class="box box-primary">
        <div class="box-header">
            <h3>
                Inserir post
            </h3>
        </div>
        <div class="box-body">
            <form method = 'POST' action = '{!!url("dashboard/post")!!}'>
                <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
                <div class="form-group">
                    <label for="titulo">Título</label>
                    <input id="titulo" name = "titulo" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="descricao">Descrição</label>
                    <input id="descricao" name = "descricao" type="text" class="form-control">
                </div>
                <button class = 'btn btn-primary' type ='submit'>Inserir</button>
            </form>
        </div>
    </div>
</div>
</section>
@endsection
