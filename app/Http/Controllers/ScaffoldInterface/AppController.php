<?php

namespace App\Http\Controllers\ScaffoldInterface;

use App\Http\Controllers\Controller;


class AppController extends Controller
{

    public function dashboard()
    {
        $users = \App\User::all()->count();
        $posts = \App\Post::all()->count();

        return view('scaffold-interface.dashboard.dashboard', compact('users', 'posts'));
    }
}
