<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class PostController.
 *
 */
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - post';
        $posts = Post::paginate(6);
        return view('dashboard.post.index',compact('posts','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - post';

        return view('dashboard.post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new Post();


        $post->titulo = $request->titulo;
        $post->descricao = $request->descricao;
        $post->save();


        return redirect('dashboard/post');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - post';

        if($request->ajax())
        {
            return URL::to('dashboard/post/'.$id);
        }

        $post = Post::findOrfail($id);
        return view('dashboard.post.show',compact('title','post'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - post';
        if($request->ajax())
        {
            return URL::to('dashboard/post/'. $id . '/edit');
        }


        $post = Post::findOrfail($id);
        return view('dashboard/post.edit',compact('title','post'  ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $post = Post::findOrfail($id);

        $post->titulo = $request->titulo;

        $post->descricao = $request->descricao;


        $post->save();

        return redirect('dashboard/post');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::BtDeleting('Atencao!!','Deseja realmente remover esse registro?','/dashboard/post/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$post = Post::findOrfail($id);
     	$post->delete();
        return URL::to('dashboard/post');
    }
}
