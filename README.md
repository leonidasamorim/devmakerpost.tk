# DevMakerPost 1.0 ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

DevMakerPost trata-se de uma aplicação web em PHP usando framework Laravel. Dispõe de módulo de controle de usuários e Post de Conteúdo, utilizando banco de dados Mysql. Desenvolvido para avaliação da empresa DevMaker www.devmaker.com.br

## Instalação

### Pré-Requisitos
* Servidor Apache
* PHP >= 7.0
* Laravel >= 5.4.*
* Banco MySQL >= 5.6

## Guia de Instalação

1. Você deve realizar a clonagem do repositório no ambiente local seguindo os pré-requisitos acima.

2. Realizar a instalação da base de dados de demostração que encontra-se no repositório na pasta:

``/storage/app/public/basedemo.sql``

3. Após isso deve alterar o arquivo ".env" que está na raiz do projeto e incluir os dados de conexão da base de dados nas variáveis abaixo:  


``DB_HOST=``
``DB_PORT=``
``DB_DATABASE=``
``DB_USERNAME=``
``DB_PASSWORD=``

## Versão de Demo
Para acessar a versão de demostração basta acessar o endereço:

**http://devmakerpost.tk/**

Você pode criar um novo usuário em:

**http://devmakerpost.tk/register**

ou acessar com usuário de demostração, em:

**http://devmakerpost.tk/login**

E-mail: demo@demo.com

Senha: password


## License
[Open Source](https://choosealicense.com/licenses/mit/)
